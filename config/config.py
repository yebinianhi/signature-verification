training_dir = "data/english/train"
training_csv = "data/english/train_data.csv"
testing_csv = "data/english/test_data.csv"
testing_dir = "data/english/test"
batch_size = 12
epochs = 20
max_lr = 0.001

model_path = "model/weights/model.pth"
model_save = "model/weights/model_3.pth"